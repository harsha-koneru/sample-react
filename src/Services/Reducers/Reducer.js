const initialData={
    toDoArray:[]
}

function cardFunction(state=initialData,action)
{
    switch(action.type){
     
        case "ADD_TO_CART":

        return{
            ...state,
            toDoArray:[
                ...state.toDoArray,
                {
                    data:action.data,
                    editMode:false,
                },
            ] , 
        };
        case "REMOVE_FROM_LIST":
             const todo=state.toDoArray.filter((items,i)=>i!==action.id);
        return{
           ...state,
           toDoArray:todo,
        };
        case "ON_EDIT_LIST":
        const toEdit= state.toDoArray.filter((items,ind)=>
                       {if(ind===action.idToEdit){
                             items.editMode=!items.editMode
                             return items
                       }
                       return items
                    }
                      
            )
       return{
          ...state,
          toDoArray:toEdit,
          
       };
       case "ADD_TO_CART_AGAIN":
        console.log("reducer",action);
        const toAdd= state.toDoArray.filter((items,ind)=>
        {if(ind===action.id){
              items.data=action.additem
              return items
        }
        return items
        })
          return{
           ...state,
             toDoArray:toAdd,
        };
        default:
            return state
    }
    
}
export default cardFunction;