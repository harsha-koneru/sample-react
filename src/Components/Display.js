import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes,faPencilAlt, faSave} from '@fortawesome/free-solid-svg-icons';
import React from 'react';
import { useState } from 'react';

const Display = (props) =>{
    
    let [inputTextDis,setInputDis]=useState('');
    const inputReaderDis= (get) =>{
        setInputDis(get.target.value);
      };
    const reinitialize=()=>{
        setInputDis('');
        console.log("gggg",inputTextDis);
    }
    const divEditOrList=()=>{
    if(props.toggle===false)
    {
        return(
            <div className="list">
            <div><b>{props.text}</b></div><div className="delete-icon" onClick={() =>{props.onSelect(props.id)}}><FontAwesomeIcon icon={faTimes} size="2x"/></div><div className="editList" onClick={() =>{props.onEditClick(props.id)}}><FontAwesomeIcon icon={faPencilAlt} size="1x" className="fapenciledit"/></div>
            </div>
        )
    }
    else
    {
        
        return(
            <div>
            <input type="text" id="name" value={inputTextDis} placeholder=" Add a Replacable item . . ." className="textDis" onChange={inputReaderDis} autocomplete="off"/>
            <div className="editList2" onClick={() =>{props.onaddToAgain(inputTextDis,props.id);reinitialize()}}><FontAwesomeIcon icon={faSave} size="2x" className="fapenciledit2"/></div>
            </div>
        )
       
    }
    
    }
   
    return (
        divEditOrList()  
    )
};

export default Display;