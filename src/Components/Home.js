import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPencilAlt,faTimes,faSave} from '@fortawesome/free-solid-svg-icons'
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import {connect} from 'react-redux';
import Error from './Error';
import Errorsub from './Errorsub';
import './Header.css'; 
import Display from './Display';
import { useState } from 'react';
import React, {useEffect } from 'react';
import addToArray from '../Services/Actions/Action';
import addAgain from '../Services/Actions/ActionAddAgain';
import OnEditList from '../Services/Actions/ActionOnEdit';
import deleteItemsList from '../Services/Actions/ActionDelete';

const Home= (props)=> {
  let [inputText,setInput]=useState('');
  let [errorMessage,updateError]=useState('');
  let  check =/^[0-9a-zA-Z _]+$/;

  const inputReader= (get) =>{
    setInput(get.target.value);

  };

  const addOnList= () =>{
                           if(inputText!==''){
                               if(inputText.length<=28){
                                     if(inputText.match(check)){
                                     
                                      props.dispatch(addToArray(inputText))
                                     }
                                   else{
                                         errorMessage='*Use Numbers & letters only';
                                         updateError(errorMessage);
                                       }
                                }else{
                                     errorMessage='*No of letters must be less than or equal to 28';
                                     updateError(errorMessage);
                                     }
                                              }
                             else{
                                 errorMessage='*Field is required';
                                 updateError(errorMessage);
                                 }
                setInput("");
  };
  console.log(props.todos.toDoArray.length);
  const deleteItems = (id) => {
    confirmAlert({
      title: 'Confirm to Delete',
      message: 'Are you sure.',
      buttons: [
        {
          label: 'Yes',
          
          onClick: ()=>{props.dispatch(deleteItemsList(id))}
        },
        {
          label: 'No'
        }
      ]
    });
     
  }
  const onEditMode=(id)=>{
    props.dispatch(OnEditList(id)) 
  }
  const againAddToList=(additem,id)=>{
      props.dispatch(addAgain(additem,id))
      props.dispatch(OnEditList(id)) 
        
  }
  useEffect(() => {setTimeout(() => {updateError(""); }, 2000);},[errorMessage]);
 
  return (
    <div className="Header">
    <header><b>WORK TO-DOS</b></header>
    <h4 className="middleHead"><b>Enter text into input field to add item to your list (max. 28 characters).</b></h4>
    <h4 className="middleHead2">Click "<FontAwesomeIcon icon={faPencilAlt}/>" to mark it as complete.</h4>
    <h4 className="middleHead3">Click " <FontAwesomeIcon icon={faTimes}/> " to remove the item from your list.</h4>
    <div>
         <input type="text" id="name" value={inputText} placeholder=" Add an Item . . ." className="text1" onChange={inputReader} autocomplete="off"/>
         <button className="pencil" onClick={addOnList}><FontAwesomeIcon icon={faSave} size="3x"/></button>
         <div className="errorm" >
          <h4>
            <Error dis={errorMessage}/>
          </h4>
        </div>
        
    </div>
    {props.todos.toDoArray.map((items,ind) => {
      return <Display key={ind} text={items.data} id={ind} toggle={items.editMode} onSelect={deleteItems} onEditClick={onEditMode} onaddToAgain={againAddToList}/>;
    })}
    <div>
    <Errorsub dis={errorMessage}/>
    </div>
    </div>

  );
}

export default connect((state=>{
  return {
    todos:state
  }}))(Home)
